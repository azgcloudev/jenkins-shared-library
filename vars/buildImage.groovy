#!/user/bin/env groovy

import com.example.Docker

def call(String imageName) { // method with a parameter
    //this = passes the context from the Docker class to the current instance
    return new Docker(this).buildDockerImage(imageName)
}