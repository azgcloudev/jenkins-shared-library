#!/user/bin/env groovy

//def call is use to call the function, and the function name for jenkins is this file name
def call() {
    echo "building the application for branch: $BRANCH_NAME"
    sh 'mvn package'
}