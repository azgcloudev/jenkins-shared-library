#!/user/bin/env groovy

package com.example

class Docker implements Serializable {
    // implements allow to keep the state if the build pauses

    // define a variable
    def script

    Docker(script) {

        // script is use to call all the commands that are use in the jenkinsfile, since they cannot be used directly
        // in the src folder
        this.script = script
    }

    def buildDockerImage(String imageName) {
        script.echo 'building the docker image...'
        script.withCredentials([script.usernamePassword(credentialsId: 'azgcloudev-dockerhub',
                passwordVariable: 'PASS', usernameVariable: 'USER')
        ]) {
            script.sh "docker build -t $imageName ."
            // quote the variables in single quotes to make sure
            // the right variable is call
            script.sh "echo '$script.PASS' | docker login -u '$script.USER' --password-stdin"
            script.sh "docker push $imageName"
        }
    }
}
